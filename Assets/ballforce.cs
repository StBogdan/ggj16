﻿using UnityEngine;
using System.Collections;

public class ballforce : MonoBehaviour {

	public int factor;
	public int jumpFactor;
	public int jumpStamina=0;
	//public int stf;
	private Rigidbody rb;
	//private CharacterController controller;
	
	void Start () {
		rb = GetComponent<Rigidbody>();
		//controller = GetComponent<CharacterController>();
	}
	
	void FixedUpdate () {
		float x = Input.GetAxis ("Horizontal");
        float z = Input.GetAxis ("Vertical");
        float y;
		if (Input.GetButton("Jump") && jumpStamina>0) {
			y = jumpFactor;
			jumpStamina--;
		}
		else {
			y=0;
			//if (jumpIteration>0) jumpIteration--; //25
		}
		
		rb.AddForce (x*factor,y,z*factor);
		//controller.Move(new Vector3 (0,0,0));
	}
	
	void OnCollisionStay (Collision collision) {
		//stf = (int) controller.collisionFlags;
		//if ((controller.Move(new Vector3 (0,0,0)) & CollisionFlags.CollidedBelow)!=0)
			jumpStamina = 25;
	}
}
